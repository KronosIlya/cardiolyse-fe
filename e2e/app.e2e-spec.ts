import { CardiolysePage } from './app.po';

describe('cardiolyse App', () => {
  let page: CardiolysePage;

  beforeEach(() => {
    page = new CardiolysePage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
