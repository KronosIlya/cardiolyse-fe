import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LoginService } from './services/login.service';
import { AlertModule } from 'ngx-bootstrap';
import { HttpModule } from '@angular/http';
import { TestService } from './services/test.service';
import { TokenService } from './services/token.service';
import { FileSelectDirective } from 'ng2-file-upload';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    FileSelectDirective
  ],
  imports: [
    BrowserModule,
    HttpModule,
    AlertModule.forRoot()
  ],
  providers: [
    LoginService,
    TestService,
    TokenService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule {
}
