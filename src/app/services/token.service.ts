import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class TokenService {
  private informAboutLoginFinished: Subject<boolean> = new Subject();

  private token = '';


  constructor() {
  }

  setToken(token: string): void {
    this.token = token;
    this.informTokenGetted();
  }

  getToken(): string {
    return this.token;
  }

  tokenAccepted(): Observable<boolean> {
    return this.informAboutLoginFinished.asObservable();
  }

  informTokenGetted(): void {
    this.informAboutLoginFinished.next(true);
  }
}
