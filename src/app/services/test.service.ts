import { Injectable } from '@angular/core';
import { TokenService } from './token.service';
import { Observable } from 'rxjs/Observable';
import { Http, Headers, Response } from '@angular/http';
import { Subscription } from 'rxjs/Subscription';
import { GeneralModel } from '../models/general.model';
import * as _ from 'lodash';
import { ReportModel } from '../models/report.model';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map';

@Injectable()
export class TestService {
    results: Subject<ReportModel> = new Subject();
    subscription: Subscription;


    private testHeaders = new Headers({
        'Content-Type': 'application/json',
    });
    private testUrl = 'https://cardiolyse-test.herokuapp.com/';


    constructor(private http: Http,
                private tokenService: TokenService) {
        this.subscription = this.tokenService.tokenAccepted()
                                .subscribe(() => {

                                });
    }

    sendFile(currentIndex: number): void {
        const body = {
            testIndex: currentIndex,
            token: this.tokenService.getToken()
        };
        this.http.post(this.testUrl + 'runTest', body, { headers: this.testHeaders })
            .subscribe((responseServer: Response) => {
                const transformedServerResponse: GeneralModel = responseServer.json() as GeneralModel;
                transformedServerResponse.key = '';
                transformedServerResponse.ecgId = '';
                transformedServerResponse.time = '';
                this.getLocalTemplate(currentIndex)
                    .subscribe((template: GeneralModel) => {
                        template.key = '';
                        template.ecgId = '';
                        template.time = '';

                        this.informReportGenerated(this.compare(transformedServerResponse, template));
                        currentIndex++;
                        if ( currentIndex <= 100 ) {
                            this.sendFile(currentIndex);
                        }
                    });
            });
    }

    getLocalTemplate(index: number): Observable<GeneralModel> {
        return this.http.get('assets/data/templates/' + index + '.json')
                   .map((response) => response.json() as GeneralModel);
    }

    compare(a: any, b: any): any {
        // a - template, b - answer from server
        var self = this;
        var result = {
            different: [],
            missing_from_template: [],
            missing_from_answer: []
        };

        _.reduce(a, function (result, value, key) {
            if ( b.hasOwnProperty(key) ) {
                if ( _.isEqual(value, b[ key ]) ) {
                    return result;
                } else {
                    if ( typeof (a[ key ]) !== typeof ({}) || typeof (b[ key ]) !== typeof ({}) ) {
                        //dead end.
                        result.different.push(key);
                        return result;
                    } else {
                        var deeper = self.compare(a[ key ], b[ key ]);
                        result.different = result.different.concat(_.map(deeper.different, (sub_path) => {
                            return key + '.' + sub_path;
                        }));

                        result.missing_from_answer = result.missing_from_answer.concat(_.map(deeper.missing_from_answer, (sub_path) => {
                            return key + '.' + sub_path;
                        }));

                        result.missing_from_template = result.missing_from_template.concat(_.map(deeper.missing_from_template, (sub_path) => {
                            return key + '.' + sub_path;
                        }));
                        return result;
                    }
                }
            } else {
                result.missing_from_answer.push(key);
                return result;
            }
        }, result);

        _.reduce(b, function (result, value, key) {
            if ( a.hasOwnProperty(key) ) {
                return result;
            } else {
                result.missing_from_template.push(key);
                return result;
            }
        }, result);

        return result;
    }

    reportGenerated(): Observable<ReportModel> {
        return this.results.asObservable();
    }

    informReportGenerated(report: ReportModel): void {
        this.results.next(report);
    }
}

