export class GeneralModel {
  key: string;
  ecgId: string;
  time: string;
  errorDesc: string;
  inference: {
    syndromic_ecg_analysis:
      {
        description: string
      }[]
  };
  basic_parameters: {
    heart_rate: string;
    rhythm: string;
    PR_interval: string;
    QRS_interval: string;
    QRS_axis: string;
    QT_interval: string;
    QTc_interval: string
  };
  all_leads: {
    'Lead III': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead V6': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead II': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead V5': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead V4': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead V3': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead AvL': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead V2': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead V1': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead AvF': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead I': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    };
    'Lead AvR': {
      QRS_uV: number;
      ST_uV: number;
      T_wave_uV: number;
      P_wave_uV: number;
    }
  };
  universal_ecg_score_system: {
    overall: string;
    stamina: string;
    myocardial: string;
    psycho_emotional: string;
    heart_rhythm_disturbances: string
  }

}

