export class ReportModel {
  different: string[];
  missing_from_template: string[];
  missing_from_answer: string[];
}
